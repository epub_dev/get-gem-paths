# get-gem-paths

This is a Node module that provides Ruby/Bundler interoperability. It's used to find the locations of installed gems in
the Jade Rails stack, which `grunt-epublishing` then uses to resolve the front-end assets it needs.

This is meant to be installed into a Jade site or engine with a Gemfile present. It uses the current working directory
to initialize Bundler and query its gem registry. It runs a Ruby script to do this, so you must have a working ruby
somewhere in your `PATH`.

## Installation & Usage

```
npm install @epublishing/get-gem-paths
```

```js
const getGemPaths = require('@epublishing/get-gem-paths');

// getGemPaths is a function that returns a promise containing an array of result objects:
getGemPaths('jade').then((results) => {
  console.log(results);
});

/**
 * each result is an object with the following shape:
 * {
 *   name: 'jade',
 *   path: '/path/to/jade',
 *   version: '6.7.pre'
 * }
 */
```
