#!/usr/bin/env node

/* eslint-disable no-console */

const prettyjson = require('prettyjson');
const getGemPaths = require('../index');
const argv = require('yargs')
  .usage('Usage: $0 <gem_pattern>')
  .example('$0 jade')
  .demand(1)
  .help('h')
  .alias('h', 'help')
  .argv;

getGemPaths(argv._[0]).then((json) => {
  console.log(prettyjson.render(json));
});
