#!/usr/bin/env ruby

require 'bundler/setup'
require 'json'

gem_search_pattern = ENV.fetch('GEM_PATTERN', ARGV[0] || 'jade')

jade_gems = Gem.loaded_specs.select { |name, _spec| name.match(/#{gem_search_pattern}/) }
path_info = jade_gems.map { |name, spec|
  {
    name: name,
    path: spec.gem_dir,
    version: spec.version.to_s,
  }
}

puts path_info.to_json
