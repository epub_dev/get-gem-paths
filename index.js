const fs    = require('fs');
const path  = require('path');
const execa = require('execa');

function findRuby() {
  const dirs = process.env.PATH.split(path.delimiter);

  for (const dir of dirs) {
    const rubyPath = path.join(dir, 'ruby');
    if (fs.existsSync(rubyPath)) return rubyPath;
  }

  return null;
}

module.exports = function getJadeGems(pattern) {
  const rubyBin = findRuby();
  const rubyScript = path.join(__dirname, 'scripts/get-gem-paths.rb');
  if (!pattern) pattern = 'jade';

  return execa(rubyBin, [ rubyScript, pattern ])
    .then((result) => JSON.parse(result.stdout));
};
